#!/bin/bash

#############################################
#                                           #
#           création du réseau              #
#                                           #
#############################################


echo "création du réseau"

az network vnet create --name la-force-tranquille-by-francois \
--resource-group OCC_ASD_Francois  \
--address-prefix 10.0.3.0/24  \
--subnet-name vm_docker_sub  \
--subnet-prefixes 10.0.3.0/28

echo "Creation du groupe de sécurité réseau"

az network nsg create \
--name security \
--resource-group OCC_ASD_Francois

sleep 30

#############################################
#                                           #
#    création groupe de sécurité            #
#                                           #
#############################################

echo "Mise en place des règles sur le groupe de sécurité"

az network nsg rule create \
--name allow_ssh \
--nsg-name security \
--priority 500 \
--resource-group OCC_ASD_Francois \
--access Allow \
--destination-port-ranges 22 \
--protocol Tcp

az network nsg rule create \
--name allow_http_https \
--nsg-name security \
--priority 501 \
--resource-group OCC_ASD_Francois \
--access Allow \
--destination-port-ranges 80 443 \
--protocol Tcp

#############################################
#                                           #
#     création de la machine virtuelle      #
#                                           #
#############################################

echo "creation de la machine virtuelle"

az vm create \
--name docker  \
--resource-group OCC_ASD_Francois  \
--vnet-name la-force-tranquille-by-francois  \
--image Debian:debian-11:11-backports-gen2:latest  \
--size Standard_B2s  \
--admin-username francois  \
--generate-ssh-keys  \
--authentication-type ssh  \
--subnet vm_docker_sub \
--nsg security

#############################################
#                                           #
#    installation de python3                #
#                                           #
#############################################

echo "installation de python3"

az vm extension set \
    --resource-group OCC_ASD_Francois \
    --vm-name docker --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "sudo apt update;sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev -y;wget https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tgz ;tar -xvf Python-3.10.0.tgz ;cd Python-3.10.0 ;sudo ./configure --enable-optimizations ;sudo make -j 2 ;sudo make altinstall "}'

#############################################
#                                           #
#    installation de docker compose         #
#                                           #
#############################################

echo "installation de docker compose"

az vm extension set \
    --resource-group OCC_ASD_Francois \
    --vm-name docker --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": " sudo apt update ; sudo curl -L \"https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose  ; sudo chmod +x /usr/local/bin/docker-compose "}'

